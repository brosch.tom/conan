from conans import ConanFile, CMake, tools


class TinyxmlConan(ConanFile):
    name = "tinyxml"
    version = "0.1"
    license = "zlib License"
    author = "Lee Thomason"
    url = "https://gitlab.pfh.research.philips.com/capputils/tinyxml"
    description = "TinyXML is a simple, small, C++ XML parser that can be easily integrating into other programs."
    topics = ("c++", "xml")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True}
    generators = "cmake"

    def source(self):
        git = tools.Git(folder=self.name)
        git.clone("git@gitlab.pfh.research.philips.com:capputils/tinyxml.git", "master")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(source_folder=self.name)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = [self.name]
